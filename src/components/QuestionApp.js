import React,{Component} from 'react'
import questions from '../jsonOfQuestions'
import Question from './Question'

class QuestionApp extends Component{
    state={
        questionNumber:0
    }
    render(){
        const {skipFunction}=this.props
        return(<div>
            <Question question={questions[this.state.questionNumber]} nextQuestion={this.buttonNextQuestion} skipFunction={skipFunction}/>

        </div>
        )
    }
    buttonNextQuestion=()=>{
        this.setState({
            questionNumber:this.state.questionNumber+1
        })
    }
}

export default QuestionApp